ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/opensource/ansible/ansible
ARG BASE_TAG=4.5.0

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

USER root

RUN dnf update -y --nodocs && \
    dnf -y clean all && \
    rm -rf /var/cache/dnf

COPY *.whl  /wheel/

RUN pip install --no-index --upgrade --find-links=/wheel/ ansible-lint && \
    rm -rf /wheel && \
    find /usr/local/lib/python3.8/site-packages/ansible_collections/ -name "*.pem" -o -name "*.key" | xargs rm -f

USER 1001 

HEALTHCHECK NONE

ENTRYPOINT ["ansible-lint"]
